# Pydew Valley Project
The goal of this project is to break down the pygame project for some young learners. Inspired by freeCodeCamp's video [Stardew Valley Game Clone with Python and Pygame – Full Course](https://www.youtube.com/watch?v=R9apl6B_ZgI)
* Source code - https://github.com/clear-code-projects/PyDew-Valley


## Getting set up

1. `git clone https://gitlab.com/mephex/pydew-valley-project `
1. `cd pydew-valley-project`
1. Activate your virtual environment
1. Install dependencies

## Implementation Notes
* The tutorial [starts out heavy](https://www.youtube.com/watch?v=R9apl6B_ZgI&t=128s), perhaps there is an easier way.
* I'll include the settings.py and main.py as they are in the video for now
* Perhaps I'll introduce a basic tutorial on how to run some simple python scripts and work up to this


